const oomph = 25; // base spin speed
const oomph_variation = 50; // max speed added by randomness
const slow_coefficient = 0.99; // smaller = faster
const stop_speed = 0.05; // speed where wheel stops

let open_section = "";

let rotation = 0;
let spin_speed = 0;
let final_rotation = 0;

let dark = "#bcd1d1";
let light = "#dde4e4";

const render = () => {
    let wheel = document.querySelector(open_section + " .wheel");
    
    wheel.style.transform = `rotate(${rotation}deg)`;

    let probability = wheel.dataset.probability;
    let degrees = (probability / 100) * 360;

    if (degrees < 180) {
        wheel.style.backgroundImage = `linear-gradient(${90 - degrees / 2}deg, ${light} 50%, transparent 50%), linear-gradient(${90 + degrees / 2}deg, transparent 50%, ${light} 50%)`;
    } else {
        wheel.style.backgroundImage = `linear-gradient(${90 - degrees / 2}deg, transparent 50%, ${dark} 50%), linear-gradient(${90 + degrees / 2}deg, transparent 50%, ${light} 50%)`;
    }

    wheel_height();

    // Rotate the pointer
    let pointer = document.querySelector(open_section + " .pointer");
    let pointer_rotation = (spin_speed / (oomph + oomph_variation)) * 90 + (rotation + (45 - final_rotation % 45)) % 45;
    pointer.style.transform = `rotate(${-pointer_rotation}deg)`;
}

const spin = () => {
    rotation += spin_speed;
    // rotation %= 360;
    spin_speed *= slow_coefficient;
    if (spin_speed > stop_speed) {
        setTimeout(spin, 1000 / 60); // 60 per second
    } else if (spin_speed === 0) {
        // so the result isn't shown when the wheel is stopped with the risk buttons
    } else {
        spin_speed = 0;
        show_result();
    }
    render();
}

const show_result = () => {
    // Show result text
    let result = document.querySelector(open_section + " .result");
    let result_text = document.querySelector(open_section + " .result_text");
    // Update result text based on wheel rotation and probability
    let wheel = document.querySelector(open_section + " .wheel");
    let probability = wheel.dataset.probability;
    let degrees = (probability / 100) * 360;
    if (rotation % 360 < degrees / 2 || rotation % 360 > 360 - degrees / 2) {
        result_text.innerHTML = "We did not survive. Let's hope the actual scenario is different.";
    } else {
        result_text.innerHTML = "Phew, we survived. Let's hope the actual scenario is similar.";
    }

    const actually_show_result = () => {
        result.classList.remove("hidden");
        // unpress spin button
        let spin_button = document.querySelector(open_section + " .spin_button");
        spin_button.classList.remove("pressed");
        // show footer
        let footer = document.querySelector("footer");
        footer.classList.remove("hidden");

        result.scrollIntoView({behavior: "smooth"});
    }

    // wait a bit and then show the result
    setTimeout(actually_show_result, 1337);
}

const start_spin = () => {
    if (spin_speed == 0) {
        spin_speed = Math.random() * oomph_variation + oomph;
        
        // hide results
        let result = document.querySelector(open_section + " .result");
        result.classList.add("hidden");
        // hide footer
        let footer = document.querySelector("footer");
        footer.classList.add("hidden");

        let spin_button = document.querySelector(open_section + " .spin_button");
        spin_button.classList.add("pressed");
        // calculate final rotation
        let temp_speed = spin_speed;
        while (temp_speed > stop_speed) {
            final_rotation += temp_speed;
            temp_speed *= slow_coefficient;
        }
        
        spin();

        // Make sure wheel is visible
        let risk_section = document.querySelector(open_section);
        risk_section.scrollIntoView({behavior: "smooth"});
    }
}

const toggle_about = () => {
    let about = document.querySelector("#about");
    about.classList.toggle("hidden");
    about.scrollIntoView({behavior: "smooth"});
    let other_risks = document.querySelector("#other_risks");
    other_risks.classList.add("hidden");
}

const toggle_other_risks = () => {
    let other_risks = document.querySelector("#other_risks");
    other_risks.classList.toggle("hidden");
    other_risks.scrollIntoView({behavior: "smooth"});
    let about = document.querySelector("#about");
    about.classList.add("hidden");
}

const wheel_height = () => {
    if (open_section) {
        let wheel = document.querySelector(open_section + " .wheel");
        wheel.style.height = wheel.offsetWidth + "px";
    }
}

window.onload = () => {
    // add risk button click behaviours
    let risk_buttons = document.querySelectorAll("#risk_buttons button");
    for (let risk_button of risk_buttons) {
        let id = risk_button.id
        let corresponding_section_id = "#" + id.substring(0, id.length - 7);
        let corresponding_section = document.querySelector(corresponding_section_id);

        risk_button.onclick = () => {
            // reset wheels
            spin_speed = 0;
            rotation = 0;
            final_rotation = 0;
            // unpress all risk buttons
            for (let other_risk_button of risk_buttons) {
                other_risk_button.classList.remove("pressed");
            }
            // hide all risk sections
            let risk_sections = document.querySelectorAll(".risk_section");
            for (let risk_section of risk_sections) {
                risk_section.classList.add("hidden");
            };
            // hide all results in risk sections
            let results = document.querySelectorAll(".result");
            for (let result of results) {
                result.classList.add("hidden");
            }
            // hide other risks
            let other_risks = document.querySelector("#other_risks");
            other_risks.classList.add("hidden");
            // hide footer
            let footer = document.querySelector("footer");
            footer.classList.add("hidden");

            // hide section if it's already open, otherwise open it
            if (open_section === corresponding_section_id) {
                open_section = "";
                corresponding_section.classList.add("hidden");
                risk_button.classList.remove("pressed");
            } else {
                open_section = corresponding_section_id;
                corresponding_section.classList.remove("hidden");
                risk_button.classList.add("pressed");
                corresponding_section.scrollIntoView({behavior: "smooth"});
            }
            render();
            // scroll to the risk section smoothly
        }
    }

    draw_charts();
}

window.onresize = () => {
    wheel_height();
}
