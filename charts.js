const font = "'Open Sans', sans-serif";
const text_color = "";

let charts = [];

const linear_ticks_callback = (value, index, values) => {
    if (value === 10000000000000) return "$10T";
    if (value === 1000000000000) return "$1T";
    if (value === 900000000000) return "$900B";
    if (value === 800000000000) return "$800B";
    if (value === 700000000000) return "$700B";
    if (value === 600000000000) return "$600B";
    if (value === 500000000000) return "$500B";
    if (value === 400000000000) return "$400B";
    if (value === 300000000000) return "$300B";
    if (value === 200000000000) return "$200B";
    if (value === 100000000000) return "$100B";
    if (value === 0) return "$0";
    return null;
}

const log_ticks_callback = (value, index, values) => {
    if (value === 10000000000000) return "$10T";
    if (value === 1000000000000) return "$1T";
    if (value === 100000000000) return "$100B";
    if (value === 10000000000) return "$10B";
    if (value === 1000000000) return "$1B";
    if (value === 100000000) return "$100M";
    if (value === 10000000) return "$10M";
    if (value === 1000000) return "$1M";
    if (value === 100000) return "$100K";
    if (value === 10000) return "$10K";
    if (value === 1000) return "$1K";
    if (value === 100) return "$100";
    if (value === 10) return "$10";
    if (value === 1) return "$1";
    if (value === 0) return "$0";
    return null;
}

let scale_type = "logarithmic";
let scale_title_text = "Yearly spending estimates (log scale)";
let scale_ticks_callback = log_ticks_callback;

const draw_charts = () => {
    Chart.defaults.font.family = font;
    Chart.defaults.color = "#34495e";

    let risk_sections = document.querySelectorAll(".risk_section");
    for (let risk_section of risk_sections) {
        let context = risk_section.querySelector(".spending_chart");
        let data = {
            datasets: [{
                labels: [
                    ["AI safety (10%)", "Low spending estimate", "$10,000,000"],
                    ["AI safety (10%)", "High spending estimate", "$100,000,000"]
                ],
                data: [{
                    x: 10,
                    y: 10000000,
                },
                {
                    x: 10,
                    y: 100000000,
                }],
                pointRadius: risk_section.id === "ai" ? 12 : 6,
                pointHoverRadius: risk_section.id === "ai" ? 16 : 8,
                backgroundColor: "#e74c3c"
            },
            {
                labels: [
                    ["Biorisk prevention (3.33%)", "Low spending estimate", "$100,000,000"],
                    ["Biorisk prevention (3.33%)", "High spending estimate", "$1,000,000,000"]
                ],
                data: [{
                    x: 3.33,
                    y: 100000000,
                },
                {
                    x: 3.33,
                    y: 1000000000,
                }],
                pointRadius: risk_section.id === "biorisk" ? 12 : 6,
                pointHoverRadius: risk_section.id === "biorisk" ? 16 : 8,
                backgroundColor: "#2ecc71"
            },
            {
                labels: [
                    ["Climate change risk prevention (0.1%)", "Low spending estimate", "$100,000,000,000"],
                    ["Climate change risk prevention (0.1%)", "High spending estimate", "$1,000,000,000,000"]
                ],
                data: [{
                    x: 0.1,
                    y: 100000000000,
                },
                {
                    x: 0.1,
                    y: 1000000000000,
                }],
                pointRadius: risk_section.id === "climatechange" ? 12 : 6,
                pointHoverRadius: risk_section.id === "climatechange" ? 16 : 8,
                backgroundColor: "#3498db"
            },
            {
                labels: [
                    ["Nuclear war risk prevention (0.1%)", "Low spending estimate", "$1,000,000,000"],
                    ["Nuclear war risk prevention (0.1%)", "High spending estimate", "$10,000,000,000"]
                ],
                data: [{
                    x: 0.1,
                    y: 1000000000,
                },
                {
                    x: 0.1,
                    y: 10000000000,
                }],
                pointRadius: risk_section.id === "nuclearwar" ? 12 : 6,
                pointHoverRadius: risk_section.id === "nuclearwar" ? 16 : 8,
                backgroundColor: "#f1c40f"
            }]
        }

        let options = {
            plugins: {
                tooltip: {
                    callbacks: {
                        label: function(ctx) {
                            // console.log(ctx);
                            let label = ctx.dataset.labels[ctx.dataIndex];
                            return label;
                        }
                    },
                    displayColors: false,
                    bodyAlign: "center",
                    padding: 10
                },
                legend: {
                    display: false,
                }
            },
            scales: {
                y: {
                    type: scale_type,
                    min: 0,
                    suggestedMin: 0,
                    suggestedMax: 1000000000000,
                    title: {
                        display: true,
                        text: scale_title_text,
                    },
                    ticks: {
                        callback: scale_ticks_callback
                    }
                },
                x: {
                    suggestedMin: 0,
                    suggestedMax: 12,
                    title: {
                        display: true,
                        text: "Probability of extinction within 100 years",
                        padding: 8,
                    },
                    ticks: {
                        callback: (value, index, values) => {
                            return value + "%";
                        }
                    }
                }
            }
        }

        let scatterChart = new Chart(context, {
            type: "scatter",
            data: data,
            options: options
        });

        charts.push(scatterChart);
    }

}

const update_charts = (charts) => {
    for (let chart of charts) {
        chart._options.scales.y.type = scale_type;
        chart._options.scales.y.ticks.callback = scale_ticks_callback;
        chart._options.scales.y.title.text = scale_title_text;
        chart.update();
    }
}

const switch_scale_types = () => {
    let scale_switch_buttons = document.querySelectorAll(".scale_switch_button");
    for (let scale_switch_button of scale_switch_buttons) {
        scale_switch_button.innerHTML = `Switch to ${scale_type} scale`;
    }
    
    if (scale_type === "linear") {
        scale_type = "logarithmic";
        scale_ticks_callback = log_ticks_callback;
        scale_title_text = "Yearly spending estimates (log scale)";
    } else {
        scale_type = "linear";
        scale_ticks_callback = linear_ticks_callback;
        scale_title_text = "Yearly spending estimates (linear scale)"
    }
    
    update_charts(charts);
};
